JSync
=====
A tool to sync apps and tweaks (from Cydia) to and from a jailbroken iDevice.

Dependencies
------------
### Build ###
* cmake
* ninja

### Runtime ###
* libimobiledevice
* libarchive
* qt5

Build and Install
-----------------
### Unix (including Mac OS X) ###
1. `cd path/to/jsync`
2. `mkdir build`
3. `cd build`
4. `cmake -G Ninja ..`
5. `ninja`
6. `sudo ninja install`

### Windows (untested) ###
1. `cd path\to\jsync`
2. `mkdir build`
3. `cd build`
4. `cmake -G Ninja ..`
5. `ninja`
6. `runas /user:Administrator "ninja install"`

License
-------
This project is licensed under the MPL v2.0.  See LICENSE for more details.