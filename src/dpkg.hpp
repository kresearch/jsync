/*
 * Copyright (C) 2013 Arcterus
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <QtCore/QVector>
#include <QtCore/QFile>
#include <QtCore/QDir>
#include <archive.h>

namespace jsync {
	namespace dpkg {
		class Ar {
			public:
			Ar(const char *filename);

			/*void addFile(const char *filename);
			void addArchive(Ar *archive);*/
			void add(const char *name);
			const char *getFileName();
			//QVector<Ar const&> getArchives();
			QVector<const char *> getFiles();
			void write();
			void read();
			void extract();

			protected:
			int (*format)(struct archive *archive);

			private:
			int copyData(struct archive *ar, struct archive *aw);

			const char *location;
			QVector<const char *> files;
			//QVector<Ar *> archives;
		};

		class Tar : public Ar {
			public:
			Tar(const char *filename);
		};

		class Deb : private Ar {
			public:
			Deb(const char *filename, const char *vers = "2.0",
				Tar *control = NULL, Tar *data = NULL);

			void setControl(Tar *archive);
			void setData(Tar *archive);
			void setVersion(char *vers);
			void write();
			void read();
			void extract();

			private:
			const char *version;
			Tar *control;
			Tar *data;
		};
	}
}