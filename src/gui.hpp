/*
 * Copyright (C) 2013 Arcterus
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef _KR_JSYNC_GUI_HPP
#define _KR_JSYNC_GUI_HPP

#include <QtWidgets/QtWidgets>

namespace jsync {
	namespace gui {
		class MainWindow : public QMainWindow {
			Q_OBJECT
			
			public:
			MainWindow();

			protected:
			//void closeEvent(QCloseEvent *event);
		};

		class WindowBody : public QWidget {
			Q_OBJECT

			public:
			WindowBody(QWidget *parent = 0);
			
			private slots:
			void startSync();
			
			private:
			void setupDeviceList(QGridLayout *layout);
			void setupProgressBar(QGridLayout *layout);
			void setupAppList(QGridLayout *layout);
			void setupFileList(QGridLayout *layout);
			
			QProgressBar *progbar;
			QLabel *label;
			QListWidget *devlist, *applist, *filelist;
			QPushButton *syncbutton;
		};
	}
	int startGui(int argc, char **argv);
}

#endif