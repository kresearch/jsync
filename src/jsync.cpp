/*
 * Copyright (C) 2013 Arcterus
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "jsync.hpp"
#include <QtCore/QDebug>
#include <QtCore/QByteArray>
#include <wordexp.h>

#define SLURP_SIZE 1024

namespace jsync {
	Device::Device(char *inUdid) {
		udid = QString(inUdid);
		idevice_new(&device, inUdid);
		lockdownd_client_t client;
		lockdownd_client_new(device, &client, "jsync");
		char *realName;
		if(lockdownd_get_device_name(client, &realName) < 0) {
			// sad face
			name = "";
		} else {
			name = QString(realName);
			free(realName);
		}
		lockdownd_client_free(client);
	}

	Device::Device() {
		// stupid, useless default constructor for Qt containers
		udid = "";
		name = "";
		device = NULL;
		afc_client = NULL;
	}

	lockdownd_service_descriptor_t Device::startService(const char *service) {
		lockdownd_client_t client = NULL;
		lockdownd_client_new_with_handshake(device, &client, "jsync");
		lockdownd_service_descriptor_t descriptor = NULL;
		lockdownd_start_service(client, service, &descriptor);
		return descriptor;
	}

	int Device::startAfc2() {
		lockdownd_service_descriptor_t descriptor = startService(
				"com.apple.afc2");
		return afc_client_new(device, descriptor, &afc_client);
	}

	int Device::sync(SyncDirection direction) {
		startAfc2();
		if(direction == DOWN) {
			return backup();
		} else {
			return -1;
			//return backdown();  // i need a better term for this
		}
		return 0;
	}

	int Device::backup() {
		QVector<QString> files = ls("/var/lib/dpkg/info");
		for(int i = 0; i < files.size(); i++) {
			if(files[i].endsWith(".list")) {
				qDebug() << files[i];
				QStringList flist = open("/var/lib/dpkg/info/" + files[i],
						AFC_FOPEN_RDONLY).slurp().split("\n");
				QString support = QString(expandTilde(SUPPORT_DIR)) + "/tmp/";
				dpkg::Tar data((support + "data.tar.xz").toUtf8().constData());
				for(int x = 0; x < flist.size(); x++) {
					QString file = support + flist[x];
					QFile(file).write(open(flist[x], AFC_FOPEN_RDONLY).slurp().
							toUtf8().constData());
					data.add(file.toUtf8().constData());
				}
				// add control down here
				dpkg::Tar control((support + "control.tar.xz").toUtf8().
						constData());
				dpkg::Deb deb(((QString(expandTilde(SUPPORT_DIR)) + "/data/") +
						files[i].left(files[i].length() - 5)).toUtf8().
							constData());
				deb.setControl(&control);
				deb.setData(&data);
				deb.write();
			}
		}
		return 0;
	}

	QVector<QString> Device::ls(const char *dir) {
		char **list = NULL;
		QVector<QString> result;
		int error = afc_read_directory(afc_client, dir, &list);
		if(error != AFC_E_SUCCESS) {
			puts("sad face");
		}
		for(int i = 0; list[i] != NULL; i++) {
			result.append(QString(list[i]));
			free(list[i]);
		}
		free(list);
		return result;
	}

	File Device::open(const char *filename, afc_file_mode_t mode) {
		return File(afc_client, filename, mode);
	}

	File Device::open(QString const& filename, afc_file_mode_t mode) {
		return open(filename.toUtf8().data(), mode);
	}

	QString Device::getName() {
		return name;
	}

	QString Device::getUdid() {
		return udid;
	}

	const char *Device::expandTilde(const char *tilde) {
		wordexp_t exp;
		wordexp(tilde, &exp, 0);
		return exp.we_wordv[0];
	}

	File::File(afc_client_t afc_client, const char *filename,
			afc_file_mode_t mode) {
		client = afc_client;
		int err = afc_file_open(client, filename, mode, &handle);
		if(err != AFC_E_SUCCESS) {
			// sad face
		}
	}

	File::~File() {
		afc_file_close(client, handle);
	}

	QString File::slurp() {
		QString result = "";
		char data[SLURP_SIZE];
		int error = 0;
		while((error = read(data, SLURP_SIZE)) != 0) {
			if(error < 0) {
				// sad face
				break;
			} else {
				result += data;
			}
		}
		return result;
	}

	void File::dump(QString const& strData) {
		QByteArray buffer = strData.toUtf8();
		write(buffer.constData(), buffer.length());
	}

	int File::read(char *data, uint32_t length) {
		uint32_t bytesRead;
		afc_error_t err = afc_file_read(client, handle, data, length,
				&bytesRead);
		if(err) {
			return -err;
		} else {
			return bytesRead;
		}
	}

	int File::write(const char *data, uint32_t length) {
		uint32_t bytesWritten;
		afc_error_t err = afc_file_write(client, handle, data, length,
				&bytesWritten);
		if(err) {
			return -err;
		} else {
			return bytesWritten;
		}
	}

	QVector<Device> deviceList() {
		char **devices = NULL;
		int count = 0;
		QVector<Device> result;
		if(idevice_get_device_list(&devices, &count) < 0) {
			fputs("Error: Unable to retrieve device list", stderr);
		} else {
			for(int i = 0; i < count; i++) {
				result.append(Device(devices[i]));
			}
			idevice_device_list_free(devices);
		}
		return result;
	}

	int test() {
		// TODO: add tests
		return 0;
	}
}