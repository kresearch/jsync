/*
 * Copyright (C) 2013 Arcterus
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef _KR_JSYNC_JSYNC_HPP

#include <QtCore/QVector>
#include <QtCore/QString>
#include <QtCore/QMetaType>
#include <libimobiledevice/libimobiledevice.h>
#include <libimobiledevice/afc.h>
#include <libimobiledevice/lockdown.h>
#include "config.h"
#include "gui.hpp"
#include "dpkg.hpp"

namespace jsync {
	enum SyncDirection {
		UP,
		DOWN
	};

	class File;

	class Device {
		public:
		Device(char *udid);
		Device();

		int startAfc2();
		lockdownd_service_descriptor_t startService(const char *service);
		int sync(SyncDirection direction);
		int backup();
		File open(const char *filename, afc_file_mode_t mode);
		File open(QString const& name, afc_file_mode_t mode);
		QVector<QString> ls(const char *dir);
		QString getName();
		QString getUdid();

		private:
		const char *expandTilde(const char *tilde);

		idevice_t device;
		afc_client_t afc_client;
		QString name;
		QString udid;
	};

	class File {
		public:
		File(afc_client_t client, const char *filename, afc_file_mode_t mode);
		~File();

		QString slurp();
		void dump(QString const& data);
		int read(char *data, uint32_t length);
		int write(const char *data, uint32_t length);

		private:
		uint64_t handle;
		afc_client_t client;
	};

	QVector<Device> deviceList();
	int test();
}

Q_DECLARE_METATYPE(jsync::Device)

#endif