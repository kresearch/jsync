/*
 * Copyright (C) 2013 Arcterus
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <cstring>
#include "jsync.hpp"

int main(int argc, char **argv) {
	if(argc > 1 && !strcmp(argv[1], "--test")) {
		// run through tests
		return jsync::test();
	} else {
		return jsync::startGui(argc, argv);
	}
}