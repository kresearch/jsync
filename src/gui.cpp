/*
 * Copyright (C) 2013 Arcterus
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "gui.hpp"
#include "jsync.hpp"

namespace jsync {
	namespace gui {
		MainWindow::MainWindow() {
			setFixedSize(640, 480);
			setCentralWidget(new WindowBody);
		}

		WindowBody::WindowBody(QWidget *parent) : QWidget(parent) {
			QGridLayout *layout = new QGridLayout;
			setLayout(layout);
			setupDeviceList(layout);
			setupProgressBar(layout);
			setupAppList(layout);
			setupFileList(layout);
		}
		
		void WindowBody::setupDeviceList(QGridLayout *layout) {
			devlist = new QListWidget;
			QVector<Device> list = deviceList();
			for(int i = 0; i < list.size(); i++) {
				QListWidgetItem *item = new QListWidgetItem(devlist);
				item->setText(list[i].getName());
				item->setData(Qt::UserRole, QVariant::fromValue(list[i]));
			}
			layout->addWidget(devlist, 0, 0, 2, 1);
		}

		void WindowBody::setupProgressBar(QGridLayout *layout) {
			QVBoxLayout *vert = new QVBoxLayout;
			progbar = new QProgressBar;
			vert->addWidget(progbar);
			QHBoxLayout *horiz = new QHBoxLayout;
			label = new QLabel(tr("Awaiting selection..."));
			label->setAlignment(Qt::AlignVCenter | Qt::AlignHCenter);
			horiz->addWidget(label);
			syncbutton = new QPushButton(tr("Sync"));
			connect(syncbutton, SIGNAL(clicked()), this, SLOT(startSync()));
			horiz->addWidget(syncbutton);
			vert->addLayout(horiz);
			layout->addLayout(vert, 0, 1, 1, 2);
		}

		void WindowBody::setupAppList(QGridLayout *layout) {
			applist = new QListWidget;
			layout->addWidget(applist, 1, 1, 1, 1);
		}

		void WindowBody::setupFileList(QGridLayout *layout) {
			filelist = new QListWidget;
			layout->addWidget(filelist, 1, 2, 1, 1);
		}

		void WindowBody::startSync() {
			label->setText(tr("Syncing to Device..."));
			syncbutton->setText(tr("Stop"));
			QListWidgetItem *item = devlist->currentItem();
			if(item) {
				item->data(Qt::UserRole).value<Device>().sync(jsync::DOWN);
			}
			syncbutton->setText(tr("Sync"));
			label->setText(tr("Awaiting selection..."));
		}
	}

	int startGui(int argc, char **argv) {
		QApplication app(argc, argv);
		gui::MainWindow window;
		window.show();
		return app.exec();
	}
}