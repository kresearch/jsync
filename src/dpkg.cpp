/*
 * Copyright (C) 2013 Arcterus
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "dpkg.hpp"
#include <archive.h>
#include <archive_entry.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>

#define BUFFER_SIZE 8192
#define DEB_VERSION "2.0"

namespace jsync {
	namespace dpkg {
		Deb::Deb(const char *filename, const char *vers,
				Tar *ctrl, Tar *dat) : Ar(filename) {
			version = vers;
			control = ctrl;
			data = dat;
		}

		void Deb::setControl(Tar *archive) {
			control = archive;
		}

		void Deb::setData(Tar *archive) {
			data = archive;
		}

		void Deb::setVersion(char *vers) {
			version = vers;
		}

		void Deb::read() {
			Ar::read();
		}

		void Deb::write() {
			control->write();
			data->write();
			FILE *vers = fopen("debian-binary", "w");
			if(vers == NULL) {
				// sad face
			}
			fputs(DEB_VERSION, vers);
			fclose(vers);
			add("debian-binary");
			add(control->getFileName());
			add(data->getFileName());
			//Ar::addArchive(control);
			//Ar::addArchive(data);
			Ar::write();
			unlink("debian-binary");
			unlink(control->getFileName());
			unlink(data->getFileName());
		}

		void Deb::extract() {
			Ar::extract();
		}

		Ar::Ar(const char *filename) {
			location = filename;
			format = &archive_write_set_format_ar_bsd;
		}

		const char *Ar::getFileName() {
			return location;
		}

		/*void Ar::addArchive(Ar *archive) {
			archives.append(archive);
		}*/

		void Ar::add(const char *file) {
			files.append(file);
		}

		void Ar::read() {

		}

		// NOTE: directories do not work with ar.
		void Ar::write() {
			struct archive *ar = archive_write_new();
			format(ar);
			archive_write_open_filename(ar, location);
			struct stat st;
			struct archive_entry *entry = archive_entry_new();
			char buffer[BUFFER_SIZE];
			for(int i = 0; i < files.size(); i++) {
				const char *filename = files[i];
				lstat(filename, &st);
				archive_entry_set_pathname(entry, filename);
				archive_entry_set_size(entry, st.st_size);
				archive_entry_set_perm(entry, st.st_mode);
				archive_entry_set_atime(entry, st.st_atime, 0);
				archive_entry_set_ctime(entry, st.st_ctime, 0);
				archive_entry_set_mtime(entry, st.st_mtime, 0);
				archive_entry_set_uid(entry, st.st_uid);
				archive_entry_set_gid(entry, st.st_gid);
				if(S_ISREG(st.st_mode)) {
					archive_entry_set_filetype(entry, AE_IFREG);
				} else if(S_ISDIR(st.st_mode)) {
					archive_entry_set_filetype(entry, AE_IFDIR);
				} else if(S_ISLNK(st.st_mode)) {
					archive_entry_set_filetype(entry, AE_IFLNK);
					ssize_t length;
					if((length = readlink(filename, buffer,
							sizeof(buffer) - 1)) != -1) {
						buffer[length] = '\0';
						archive_entry_set_symlink(entry, buffer);
					} else {
						archive_entry_set_symlink(entry, NULL);
					}
				}
				archive_write_header(ar, entry);
				int fd = open(filename, O_RDONLY);
				int length = ::read(fd, buffer, sizeof(buffer));
				while(length > 0) {
					archive_write_data(ar, buffer, length);
					length = ::read(fd, buffer, sizeof(buffer));
				}
				close(fd);
				archive_entry_clear(entry);
			}
			archive_entry_free(entry);
			archive_write_close(ar);
			archive_write_free(ar);
		}

		// NOTE: this should work with .tar files too
		void Ar::extract() {
			int flags = ARCHIVE_EXTRACT_TIME | ARCHIVE_EXTRACT_PERM |
			            ARCHIVE_EXTRACT_ACL | ARCHIVE_EXTRACT_FFLAGS;
			struct archive *ar = archive_read_new();
			archive_read_support_format_all(ar);
			archive_read_support_filter_bzip2(ar);
			archive_read_support_filter_gzip(ar);
			archive_read_support_filter_lzma(ar);
			archive_read_support_filter_xz(ar);
			struct archive *ext = archive_write_disk_new();
			archive_write_disk_set_options(ext, flags);
			archive_write_disk_set_standard_lookup(ext);
			int r = archive_read_open_filename(ar, location, 10240);
			if(r) {
				// error
			}
			struct archive_entry *entry;
			while((r = archive_read_next_header(ar, &entry)) != ARCHIVE_EOF) {
				if(r != ARCHIVE_OK) {
					// error
					fputs(archive_error_string(ar), stderr);
				}
				if(r < ARCHIVE_WARN) {
					// error
				} else if((r = archive_write_header(ext,
						entry)) != ARCHIVE_OK) {
					// error
					fputs(archive_error_string(ext), stderr);
				} else if(archive_entry_size(entry) > 0) {
					copyData(ar, ext);
					if(r != ARCHIVE_OK) {
						// error
						fputs(archive_error_string(ext), stderr);
					}
					if(r < ARCHIVE_WARN) {
						// error
					}
				}
				if((r = archive_write_finish_entry(ext)) != ARCHIVE_OK) {
					// error
					fputs(archive_error_string(ext), stderr);
				}
				if(r < ARCHIVE_WARN) {
					// error
				}
			}
			archive_read_close(ar);
			archive_read_free(ar);
			archive_write_close(ext);
			archive_write_free(ext);
		}

		int Ar::copyData(struct archive *ar, struct archive *aw) {
			int r;
			const void *buffer;
			size_t size;
			off_t offset;
			while(true) {
				r = archive_read_data_block(ar, &buffer, &size, &offset);
				if(r == ARCHIVE_EOF) {
					return ARCHIVE_OK;
				} else if(r != ARCHIVE_OK) {
					return r;
				} else if((r = archive_write_data_block(aw, buffer, size,
						offset)) != ARCHIVE_OK) {
					// error
					fputs(archive_error_string(aw), stderr);
					return r;
				}
			}
		}

		Tar::Tar(const char *filename) : Ar(filename) {
			format = archive_write_set_format_ustar;
		}
	}
}